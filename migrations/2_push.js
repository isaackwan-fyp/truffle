var Registry = artifacts.require("./Registry.sol");
var MakeClaim = artifacts.require("./MakeClaim.sol");

module.exports = function(deployer) {
	deployer.deploy(Registry);
	deployer.deploy(MakeClaim);
};
