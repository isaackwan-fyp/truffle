pragma solidity ^0.4.20;
pragma experimental ABIEncoderV2;

contract CompetitionMother {
    CompetitionRegistry[] public competitions;
    
    event StartedCompetition (
        address indexed _from,
        CompetitionRegistry registry
    );
    
    function create(string description) public payable {
        CompetitionRegistry registry = (new CompetitionRegistry).value(msg.value)(description);
        competitions.push(registry);
        emit StartedCompetition(msg.sender, registry);
    }
    
    function competitionsLength() public view returns (uint) {
        return competitions.length;
    }
}

contract CompetitionRegistry {
    Contestant[] public claims;
    mapping(address => uint) public claims_check;
    Registry public registry;
    mapping (string => Contestant[]) claims_key;
	mapping (address => Contestant[]) claims_user;
	mapping (address => address) public voted;
	uint public fund = 0; // in wei
	uint public total = 0; // number of people who have voted across all contestants
	string public description;

	event Claimed(
		address indexed _from,
		int indexed nonce,
		Contestant claim
    );
    
    function CompetitionRegistry(string description_) payable public {
        description = description_;
        // registry = new Registry();
    }

    function claim(string assertion, string key, int nonce) public {
        Contestant newContract = new Contestant(assertion, msg.sender, this, key);
		// newContract.topUp.value(msg.value)();
		fund = msg.value;
        claims.push(newContract);
		claims_check[address(newContract)] = 1;
		claims_key[key].push(newContract);
		claims_user[msg.sender].push(newContract);
		// newContract.voteUp();
        Claimed(msg.sender, nonce, newContract);
    }
    
    function claimsLength() view public returns(uint) {
        return claims.length;
    }

	function getClaimsLengthByKey(string key) view public returns(uint) {
		return claims_key[key].length;
	}

	function getClaimAddressByKeyAndIndex(string key, uint index) view public returns (Contestant) {
	    return claims_key[key][index];
	}

	function getClaimsLengthByUser(address userAddress) view public returns(uint) {
		return claims_user[userAddress].length;
	}

	function getClaimAddressByUserAndIndex(address userAddress, uint index) view public returns (Contestant) {
	    return claims_user[userAddress][index];
	}

	function payout(address to) public returns(uint) {
		require(voted[to] == 0);
		voted[to] = msg.sender;

        total += 1;
        // x*0.381966011^x 
        uint value = (0.381966011 ether ** total) * total * fund;
        value /= 1 ether;
        to.transfer(value);
        return value;
    }
}

contract Contestant {
    string public key;
    string public assertion;
	address public by;
	CompetitionRegistry registry;
	address[] public votedUp;
	mapping (address => uint) votedUpList;
	address[] public votedDown;
	mapping (address => uint) votedDownList;
	uint256 public timestamp;
    
    event Paid(
		address indexed _to,
		uint value
    );

    event VoteRejected(
		address indexed _from
    );

    function Contestant (string assertion_, address by_, CompetitionRegistry registry_, string key_) public payable {
        assertion = assertion_;
		by = by_;
		key = key_;
		registry = registry_;
		timestamp = now;
		// registry.register();
    }

	function voteUp() public {
		if (hasVoted()) {
			VoteRejected(msg.sender);
		} else {
			votedUpList[msg.sender] = 1;
			votedUp.push(msg.sender);
			payout();
		}
	}

	function votedUpLength() view public returns (uint) {
		return votedUp.length;
	}

	function voteDown() public {
		if (hasVoted()) {
			VoteRejected(msg.sender);
		} else {
			votedDownList[msg.sender] = 1;
			votedDown.push(msg.sender);
			payout();
		}
	}

	function votedDownLength() view public returns (uint) {
		return votedDown.length;
	}

	function hasVoted() view internal returns (bool) {
		return false;
	}

	function payout() internal {
        Paid(msg.sender, registry.payout(msg.sender));
    }
}

contract MakeClaim {
    Claim[] public claims;
    Registry public registry;
    mapping (string => Claim[]) claims_key;
	mapping (address => Claim[]) claims_user;

	event Claimed(
		address indexed _from,
		Claim claim
    );
    
    function MakeClaim() public {
        registry = new Registry();
    }

    function claim(string assertion, string key) payable public {
        Claim newContract = new Claim(assertion, msg.sender, registry, key);
		newContract.topUp.value(msg.value)();
        claims.push(newContract);
		claims_key[key].push(newContract);
		claims_user[msg.sender].push(newContract);
		// newContract.voteUp();
        Claimed(msg.sender, newContract);
    }
    
    function claimsLength() view public returns(uint) {
        return claims.length;
    }

	function getClaimsLengthByKey(string key) view public returns(uint) {
		return claims_key[key].length;
	}

	function getClaimAddressByKeyAndIndex(string key, uint index) view public returns (Claim) {
	    return claims_key[key][index];
	}

	function getClaimsLengthByUser(address userAddress) view public returns(uint) {
		return claims_user[userAddress].length;
	}

	function getClaimAddressByUserAndIndex(address userAddress, uint index) view public returns (Claim) {
	    return claims_user[userAddress][index];
	}
}

contract Claim {
    string public key;
    string public assertion;
	address public by;
	Registry registry;
	address[] public votedUp;
	mapping (address => uint) votedUpList;
	address[] public votedDown;
	mapping (address => uint) votedDownList;
	uint256 public timestamp;
	uint public fund = 0; // in wei
    
    event Paid(
		address indexed _to,
		uint value
    );

    event VoteRejected(
		address indexed _from
    );

    function Claim (string assertion_, address by_, Registry registry_, string key_) public payable {
        assertion = assertion_;
		by = by_;
		key = key_;
		registry = registry_;
		timestamp = now;
		// registry.register();
    }

	function topUp() public payable {
        fund += msg.value;
    }

	function voteUp() public {
		if (hasVoted()) {
			VoteRejected(msg.sender);
		} else {
			votedUpList[msg.sender] = 1;
			votedUp.push(msg.sender);
			payout();
		}
	}

	function votedUpLength() view public returns (uint) {
		return votedUp.length;
	}

	function voteDown() public {
		if (hasVoted()) {
			VoteRejected(msg.sender);
		} else {
			votedDownList[msg.sender] = 1;
			votedDown.push(msg.sender);
			payout();
		}
	}

	function votedDownLength() view public returns (uint) {
		return votedDown.length;
	}

	function hasVoted() view internal returns (bool) {
		return votedUpList[msg.sender] != 0 || votedDownList[msg.sender] != 0;
	}

	function payout() internal {
        uint total = votedUp.length + votedDown.length;
        // x*0.381966011^x 
        uint value = (0.381966011 ether ** total) * total * fund;
        value /= 1 ether;
        msg.sender.transfer(value);
        Paid(msg.sender, value);
    }
}

contract Registry {
    // feeling puzzled? see https://notebooks.azure.com/n/pEdBqBdBlOI/notebooks/pagerank-solidity_low2.ipynb, https://notebooks.azure.com/n/pEdBqBdBlOI/notebooks/pagerank-solidity_low.ipynb and https://notebooks.azure.com/n/pEdBqBdBlOI/notebooks/pagerank-solidity_high.ipynb
	bool[][] matrix;
	mapping (address => uint) public id_mapping;
	mapping (address => string) public names;
	mapping (address => string) public avatars;

	event Registerd(
		address indexed _from,
		uint id
    );

	function register(string name, string avatar) public returns (uint id) {
		require(id_mapping[msg.sender] == 0); // Existing users do not need to call register()
		
		for (uint i = 0; i < matrix.length; i++) {
			matrix[i].push(false);
		}
		bool[] memory new_record = new bool[](matrix.length + 1);
		id = matrix.push(new_record) - 1;
		id_mapping[msg.sender] = id;
		Registerd(msg.sender, id);
		names[msg.sender] = name;
		avatars[msg.sender] = avatar;
	}

	event Friended(
		address indexed _from,
		address indexed _to
    );

	function friend(address other_user) public {
		//require(id_mapping[msg.sender] != 0); // The caller must have signed up
		//require(id_mapping[other_user] != 0); // The other user must have signed up
		matrix[id_mapping[msg.sender]][id_mapping[other_user]] = true;
		Friended(msg.sender, other_user);
	}

	event Unfriended(
		address indexed _from,
		address indexed _to
    );

	function unfriend(address other_user) public {
		//require(id_mapping[msg.sender] != 0); // The caller must have signed up
		//require(id_mapping[other_user] != 0); // The other user must have signed up
		matrix[id_mapping[msg.sender]][id_mapping[other_user]] = false;
		Unfriended(msg.sender, other_user);
	}

	function isFriend(address user1, address user2) public constant returns (bool) {
		//require(id_mapping[user1] != 0); // user1 must have signed up
		//require(id_mapping[user2] != 0); // user2 must have signed up
		return matrix[id_mapping[user1]][id_mapping[user2]];
	}

	/**
	 * Computes the PageRank of a particular user.
	 * The PR is normalized such that sum of all PR's = 1.
	 *
	 * @return the normalized PR * 1,000
	 */
	function pagerank(address user1) public constant returns(uint) {
		//require(id_mapping[user1] != 0); // user1 must have signed up
		uint id1 = id_mapping[user1];
		
		// a bunch of constants
		uint24 dumping_factor = 150000;

		// variables for later use
		uint i;
		uint j;

		// compute 'total' list for normalization
		uint[] memory total = new uint[](matrix.length);
		for (i = 0; i < matrix.length; i++) {
			for (j = 0; j < matrix.length; j++) {
				total[i] += matrix[j][i] ? 1 : 0;
			}
			if (total[i] == 0) {
				for (j = 0; j < matrix.length; j++) {
					matrix[j][i] = true;
				}
				total[i] = matrix.length;
			}
		}

		// compute M2
		uint[][] memory m2 = new uint[][](matrix.length);
		for (i = 0; i < matrix.length; i++) {
			m2[i] = new uint[](matrix.length);
			for (j = 0; j < matrix.length; j++) {
				m2[i][j] = dumping_factor * (matrix[i][j] ? 1 : 0) / total[j] + (1000000-dumping_factor) / matrix.length;
			}
		}

		// actual computation
		uint[] memory PR = new uint[](matrix.length);
		
		for (i = 0; i < matrix.length; i++) {
			PR[i] = 1000000;
		}

		for (i = 0; i < 10; i++) {
			multiplyMatrix(m2, PR);
		}

		/*
		uint total2 = 0;
		for (i = 0; i < matrix.length; i++) {
			total2 += PR[i];
		}

		// normalize them
		for (i = 0; i < matrix.length; i++) {
			PR[i] *= 1000000/total2;
		}*/

		return PR[id1];
		//return normalizePagerank(PR, id1);
		
	}

	/**
	 * multiplies m1 by m2, and then stores the result to m1
	 */
	function multiplyMatrix(uint[][] memory m1, uint[] memory m2) internal view {
		uint i;
		uint j;

		uint[] memory m3 = new uint[](m2.length);

		for (i = 0; i < m2.length; i++) {
			for (j = 0; j < m2.length; j++) {
				m3[i] += (m1[i][j] * m2[j]) / 1000000;
			}
		}
		
		for (i = 0; i < m2.length; i++) {
			m2[i] = m3[i];
		}
	}
}