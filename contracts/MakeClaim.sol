pragma solidity ^0.4.20;
pragma experimental ABIEncoderV2;

contract MakeClaim {
    address[] public claims;
    Registry public registry;

	event Claimed(
		address indexed _from,
		Claim claim
    );
    
    function MakeClaim() public {
        registry = new Registry();
    }

    function claim(string assertion) payable public {
        Claim newContract = new Claim(assertion, msg.sender, registry);
        newContract.topUp.value(msg.value)();
        claims.push(newContract);
        Claimed(msg.sender, newContract);
    }
    
    function claimsLength() view public returns(uint) {
        return claims.length;
    }
}

contract Claim {
    string public assertion;
	address public by;
	Registry registry;
    uint public fund = 0;
    uint public total = 0;
    uint public total_up = 0;
    uint public total_down = 0;
    mapping (address => uint) public vote_up;
    mapping (address => uint) public vote_down;
    
    event Paid(
		address indexed _to,
		uint value
    );

    function Claim (string assertion_, address by_, Registry registry_) public payable {
        assertion = assertion_;
		by = by_;
		registry = registry_;
		registry.register();
    }

    function topUp() public payable {
        fund += msg.value;
    }

    function voteUp() public {
        noVoteBefore();
        vote_up[msg.sender] = 1;
        total_up += 1;
        payout();
    }

    function voteDown() public {
        noVoteBefore();
        vote_down[msg.sender] = 1;
        total_down += 1;
        payout();
    }

    function noVoteBefore() view internal {
        require(vote_up[msg.sender] == 0);
        require(vote_down[msg.sender] == 0);
    }

    function payout() internal {
        total += 1;
        // x*0.381966011^x 
        uint value = (0.381966011 ether ** total) * total * fund;
        value /= 1 ether;
        msg.sender.transfer(value);
        Paid(msg.sender, value);
    }
}

contract Registry {
    // feeling puzzled? see https://notebooks.azure.com/n/pEdBqBdBlOI/notebooks/pagerank-solidity_low2.ipynb, https://notebooks.azure.com/n/pEdBqBdBlOI/notebooks/pagerank-solidity_low.ipynb and https://notebooks.azure.com/n/pEdBqBdBlOI/notebooks/pagerank-solidity_high.ipynb
	bool[][] matrix;
	mapping (address => uint) public id_mapping;

	event Registerd(
		address indexed _from,
		uint id
    );

	function register() public returns (uint id) {
		require(id_mapping[msg.sender] == 0); // Existing users do not need to call register()
		
		for (uint i = 0; i < matrix.length; i++) {
			matrix[i].push(false);
		}
		bool[] memory new_record = new bool[](matrix.length + 1);
		id = matrix.push(new_record) - 1;
		id_mapping[msg.sender] = id;
		Registerd(msg.sender, id);
	}

	event Friended(
		address indexed _from,
		address indexed _to
    );

	function friend(address other_user) public {
		//require(id_mapping[msg.sender] != 0); // The caller must have signed up
		//require(id_mapping[other_user] != 0); // The other user must have signed up
		matrix[id_mapping[msg.sender]][id_mapping[other_user]] = true;
		Friended(msg.sender, other_user);
	}

	event Unfriended(
		address indexed _from,
		address indexed _to
    );

	function unfriend(address other_user) public {
		//require(id_mapping[msg.sender] != 0); // The caller must have signed up
		//require(id_mapping[other_user] != 0); // The other user must have signed up
		matrix[id_mapping[msg.sender]][id_mapping[other_user]] = false;
		Unfriended(msg.sender, other_user);
	}

	function isFriend(address user1, address user2) public constant returns (bool) {
		//require(id_mapping[user1] != 0); // user1 must have signed up
		//require(id_mapping[user2] != 0); // user2 must have signed up
		return matrix[id_mapping[user1]][id_mapping[user2]];
	}

	/**
	 * Computes the PageRank of a particular user.
	 * The PR is normalized such that sum of all PR's = 1.
	 *
	 * @return the normalized PR * 1,000
	 */
	function pagerank(address user1) public constant returns(uint) {
		//require(id_mapping[user1] != 0); // user1 must have signed up
		uint id1 = id_mapping[user1];
		
		// a bunch of constants
		uint24 dumping_factor = 150000;

		// variables for later use
		uint i;
		uint j;

		// compute 'total' list for normalization
		uint[] memory total = new uint[](matrix.length);
		for (i = 0; i < matrix.length; i++) {
			for (j = 0; j < matrix.length; j++) {
				total[i] += matrix[j][i] ? 1 : 0;
			}
			if (total[i] == 0) {
				for (j = 0; j < matrix.length; j++) {
					matrix[j][i] = true;
				}
				total[i] = matrix.length;
			}
		}

		// compute M2
		uint[][] memory m2 = new uint[][](matrix.length);
		for (i = 0; i < matrix.length; i++) {
			m2[i] = new uint[](matrix.length);
			for (j = 0; j < matrix.length; j++) {
				m2[i][j] = dumping_factor * (matrix[i][j] ? 1 : 0) / total[j] + (1000000-dumping_factor) / matrix.length;
			}
		}

		// actual computation
		uint[] memory PR = new uint[](matrix.length);
		
		for (i = 0; i < matrix.length; i++) {
			PR[i] = 1000000;
		}

		for (i = 0; i < 10; i++) {
			multiplyMatrix(m2, PR);
		}

		/*
		uint total2 = 0;
		for (i = 0; i < matrix.length; i++) {
			total2 += PR[i];
		}

		// normalize them
		for (i = 0; i < matrix.length; i++) {
			PR[i] *= 1000000/total2;
		}*/

		return PR[id1];
		//return normalizePagerank(PR, id1);
		
	}

	/**
	 * multiplies m1 by m2, and then stores the result to m1
	 */
	function multiplyMatrix(uint[][] memory m1, uint[] memory m2) internal {
		uint i;
		uint j;

		uint[] memory m3 = new uint[](m2.length);

		for (i = 0; i < m2.length; i++) {
			for (j = 0; j < m2.length; j++) {
				m3[i] += (m1[i][j] * m2[j]) / 1000000;
			}
		}
		
		for (i = 0; i < m2.length; i++) {
			m2[i] = m3[i];
		}
	}
}