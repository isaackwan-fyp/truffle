pragma solidity ^0.4.18;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/Registry.sol";

contract TestRegistry {

	function testNewRegistration() {
		Registry registry = new Registry();

		uint id = registry.register();

		Assert.equal(id, 0, "New ID equal to zero");
	}

	function testTwoNewRegistrations() {
		Registry registry = new Registry();

		uint id1 = registry.register();
		uint id2 = registry.register();
		uint id3 = registry.register();
		uint id4 = registry.register();

		Assert.equal(id1, 0, "New ID equal to zero");
		Assert.equal(id2, 1, "New ID equal to one");
		Assert.equal(id3, 2, "New ID equal to two");
		Assert.equal(id4, 3, "New ID equal to three");
	}

	function testNewUsersArentFriends() {
		Registry registry = new Registry();

		uint id1 = registry.register();
		uint id2 = registry.register();

		Assert.isFalse(registry.isFriend(0, 1), "new users aren't friends by default");
	}

	function testCanAddFriends() {
		Registry registry = new Registry();

		uint id1 = registry.register();
		uint id2 = registry.register();
		uint id3 = registry.register();

		Assert.isFalse(registry.isFriend(id1, id2), "new users aren't friends by default");

		registry.friend(id1, id2);

		Assert.isTrue(registry.isFriend(id1, id2), "but they are friends after friend-ing");
		Assert.isFalse(registry.isFriend(id3, id2), "unrelated people aren't friends");
	}

	function testPageRank() {
		Registry registry = new Registry();

		uint id1 = registry.register();
		uint id2 = registry.register();
		uint id3 = registry.register();
		uint id4 = registry.register();

		registry.friend(id1, id2);
		registry.friend(id1, id3);
		registry.friend(id1, id4);
		registry.friend(id3, id4);

		Assert.equal(registry.pagerank(0, 0), 999, "PageRank");
		Assert.equal(registry.pagerank(1, 0), 999, "PageRank");
		Assert.equal(registry.pagerank(2, 3), 999, "PageRank");
		Assert.equal(registry.pagerank(3, 3), 999, "PageRank");
	}

}
