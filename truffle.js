module.exports = {
	networks: {
		testrpc: {
			host: "localhost",
			port: 8546,
			network_id: "*", // Match any network id
			gas: 4000000,
		},
		testrpc2: {
			host: "localhost",
			port: 8547,
			network_id: "*", // Match any network id
			gas: 2000000,
			gasPrice: 6721975,
		},
		testnet: {
			host: "localhost",
			port: 8545,
			network_id: "*", // Match any network id
			//from: "0x0b2fc7dc5ca6dbdd1066c636685646c5dc587490",
			gas: 4000000,
			gasPrice: 14000000000,
		},
		rinkeby: {
			host: "localhost",
			port: 8548,
			network_id: "4",
			from: "0xaacedc79a0936eea8533b823e0e7cd5c1c96f18d",
			gas: 7000000,
			gasPrice: 19000000000,
		},
	}
};
